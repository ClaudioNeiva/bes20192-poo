package br.ucsal.bes20192.poo.aula14;

import java.util.Scanner;

public class ContatoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		try {
			cadastrarContato();
		}catch (Exception e) {
			System.out.println("Deu um erro bisonho... n�o deu pra continuar... foi mal ... at� a pr�xima...");
		}
	}

	private static void cadastrarContato() {
		TipoContatoEnum tipoContato = obterTipoContato();
		String nome = obterNome();

		System.out.println("O contato informado foi: " + nome + " do tipo " + tipoContato);
	}

	private static TipoContatoEnum obterTipoContato() {
		TipoContatoEnum tipoContato;
		while (true) {
			System.out.println("Informe o tipo do contato (pessoal ou profissional):");
			String tipoContatoString = sc.nextLine();
			
			try {
				// Converter tipoContatoString p/ tipoContato
				tipoContato = TipoContatoEnum.valueOf(tipoContatoString.toUpperCase());
				System.out.println("tipo de contato:" + tipoContato);
				return tipoContato;
			} catch (IllegalArgumentException e) {
				System.out.println("Capturei a exce��o espec�fica !.");
			}
		}
	}

	private static String obterNome() {
		System.out.println("Informe o nome:");
		return sc.nextLine();
	}

}
