package br.ucsal.bes20192.poo.aula14;

public class Veiculo {

	private String placa;

	private Double valor;

	public Veiculo(String placa, Double valor) {
		super();
		this.placa = placa;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
