package br.ucsal.bes20192.poo.aula14;

public class ExemploVeiculo {

	public static void main(String[] args) {

		VeiculoCarga veiculoCarga1 = new VeiculoCarga("ABC123", 20000d, 4);

		validarVeiculo(veiculoCarga1);
	}

	private static void validarVeiculo(Veiculo veiculo) {
		if (veiculo instanceof VeiculoCarga) {
			// Seus controles s�o mais r�gidos na hora de alugar....
		}
	}

}
