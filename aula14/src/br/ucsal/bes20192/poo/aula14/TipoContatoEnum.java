package br.ucsal.bes20192.poo.aula14;

public enum TipoContatoEnum {

	PESSOAL("pessoal, da fam�lia, amigos", 20, false),

	PROFISSIONAL("profissional, colega de trabalho", 25, true);

	private String descricao;

	private Integer idadeMinima;

	private Boolean situacao;

	private TipoContatoEnum(String descricao, Integer idadeMinima, Boolean situacao) {
		this.descricao = descricao;
		this.idadeMinima = idadeMinima;
		this.situacao = situacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getIdadeMinima() {
		return idadeMinima;
	}

	public Boolean getSituacao() {
		return situacao;
	}

}
