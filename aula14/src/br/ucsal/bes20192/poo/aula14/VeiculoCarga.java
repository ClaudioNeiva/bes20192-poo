package br.ucsal.bes20192.poo.aula14;

public class VeiculoCarga extends Veiculo {

	private Integer qtdEixos;

	public VeiculoCarga(String placa, Double valor, Integer qtdEixos) {
		super(placa, valor);
		this.qtdEixos = qtdEixos;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((qtdEixos == null) ? 0 : qtdEixos.hashCode());
		return result;
	}

}
