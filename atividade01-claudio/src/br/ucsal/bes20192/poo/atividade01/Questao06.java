package br.ucsal.bes20192.poo.atividade01;

import java.util.Scanner;

/*
6. Crie um programa em Java que permita a entrada de 10 valores em cada um dos dois
vetores (vet1 e vet2). A seguir o programa dever� armazenar num terceiro vetor (vet3) a soma
do conte�do dos dois vetores (vet1 e vet2). Por fim, o programa dever� exibir os valores
armazenados em vet3.
*/
public class Questao06 {

	private static final int QTD_ITENS = 10;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int[] vet1;
		int[] vet2;
		int[] vet3;

		vet1 = obterValores();
		vet2 = obterValores();

		vet3 = somarVetores(vet1, vet2);

		exibirVetor(vet3);
	}

	private static void exibirVetor(int[] vet) {
		System.out.println("Vetor resultado=");
		for (int i = 0; i < QTD_ITENS; i++) {
			System.out.print(vet[i] + "\t");
		}
	}

	private static int[] somarVetores(int[] vet1, int[] vet2) {
		int[] vetResultado = new int[QTD_ITENS];
		for (int i = 0; i < QTD_ITENS; i++) {
			vetResultado[i] = vet1[i] + vet2[i];
		}
		return vetResultado;
	}

	private static int[] obterValores() {
		int[] vet = new int[QTD_ITENS];
		System.out.println("Informe " + QTD_ITENS + " n�meros:");
		for (int i = 0; i < QTD_ITENS; i++) {
			vet[i] = sc.nextInt();
		}
		return vet;
	}

}
