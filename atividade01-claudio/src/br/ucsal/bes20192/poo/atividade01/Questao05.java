package br.ucsal.bes20192.poo.atividade01;

import java.util.Scanner;

/*
5. Crie um programa em Java que l� um valor N, inteiro e positivo, calcula e escreve o valor de
E (soma dos inversos dos fatoriais de 0 a N):


E = 1 / 0! + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!


E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!
Exemplo 1
Se N = 5, ent�o:
E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + 1 / 4! + 1 / 5!
Exemplo 2
Se N = 0, ent�o:
E = 1
Exemplo 3
Se N = 1, ent�o:
E = 1 + 1 / 1! 
 */

public class Questao05 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Integer n;
		Double e;

		n = obterNumero();
		e = calcularE(n);
		exibirE(n, e);

	}

	private static Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / calcularFatorial(i);
		}
		return e;
	}

	private static Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	private static void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

	private static Integer obterNumero() {
		Integer n;
		System.out.println("Informe um n�mero inteiro e positivo:");
		n = scanner.nextInt();
		return n;
	}

}
