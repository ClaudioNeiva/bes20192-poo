package br.ucsal.bes20192.poo.aula16.tui;

import java.util.Scanner;

import br.ucsal.bes20192.poo.aula16.business.AlunoBO;
import br.ucsal.bes20192.poo.aula16.domain.Aluno;
import br.ucsal.bes20192.poo.aula16.exception.NegocioException;

public class AlunoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void cadastrar() {
		Integer matricula;
		String nome;
		Integer anoNascimento;

		System.out.println("*********** CADASTRO - ALUNO ***********");

		System.out.println("Informe a matr�cula:");
		matricula = sc.nextInt();
		sc.nextLine();
		System.out.println("Informe o nome:");
		nome = sc.nextLine();
		System.out.println("Informe o ano de nascimento:");
		anoNascimento = sc.nextInt();
		sc.nextLine();

		Aluno aluno = new Aluno(matricula, nome, anoNascimento);
		AlunoBO.cadastrar(aluno);

	}

	public static void pesquisar() {
		String nome;

		System.out.println("*********** PESQUISA - ALUNO ***********");
		System.out.println("Informe o nome para pesquisa:");
		nome = sc.nextLine();

		Aluno aluno = null;
		try {
			aluno = AlunoBO.pesquisar(nome);
			System.out.println(aluno);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		} finally {
			// Voc� coloca comandos que devem ser sempre executados, independente de
			// ocorrerem erros, inclusive no catch. 
		}

	}
}
