package br.ucsal.bes20192.poo.aula16.business;

import br.ucsal.bes20192.poo.aula16.domain.Aluno;
import br.ucsal.bes20192.poo.aula16.exception.NegocioException;
import br.ucsal.bes20192.poo.aula16.persistence.AlunoDAO;

public class AlunoBO {

	public static void cadastrar(Aluno aluno) {
		validar(aluno);
		AlunoDAO.incluir(aluno);
	}

	private static void validar(Aluno aluno) {
		// Aqui podemos colocar validações.
	}

	public static Aluno pesquisar(String nome) throws NegocioException {
		return AlunoDAO.pesquisar(nome);
	}

}
