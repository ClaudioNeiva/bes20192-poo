package br.ucsal.bes20192.poo.aula16.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 8343373585644696090L;

	public NegocioException(String message) {
		super(message);
	}

}
