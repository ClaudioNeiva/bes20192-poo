package br.ucsal.bes20192.poo.aula16.exception;

public class RegistroNaoEncontradoException extends Exception {

	private static final long serialVersionUID = -7158702293061255425L;

	public RegistroNaoEncontradoException(String message) {
		super(message);
	}

}
