package br.ucsal.bes20192.poo.aula16.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20192.poo.aula16.domain.Aluno;
import br.ucsal.bes20192.poo.aula16.exception.NegocioException;
import br.ucsal.bes20192.poo.aula16.exception.RegistroNaoEncontradoException;

public class AlunoDAO {

	private static List<Aluno> alunos = new ArrayList<>();

	public static void incluir(Aluno aluno) {
		alunos.add(aluno);
	}

	public static Aluno pesquisar(String nome) throws NegocioException {
		for (Aluno aluno : alunos) {
			if (aluno.getNome().equalsIgnoreCase(nome)) {
				return aluno;
			}
		}
		throw new NegocioException("Aluno " + nome + " n�o encontrado.");
	}

}
