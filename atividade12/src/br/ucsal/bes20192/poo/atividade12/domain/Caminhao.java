package br.ucsal.bes20192.poo.atividade12.domain;

import br.ucsal.bes20192.poo.atividade12.enums.TipoCargaEnum;

public class Caminhao extends Veiculo {

	private Integer quantidadeEixos;

	private TipoCargaEnum tipoCarga;

	public Caminhao(String placa, Integer anoFabricacao, Double valor, Integer quantidadeEixos,
			TipoCargaEnum tipoCarga) {
		super(placa, anoFabricacao, valor);
		this.quantidadeEixos = quantidadeEixos;
		this.tipoCarga = tipoCarga;
	}

	public Integer getQuantidadeEixos() {
		return quantidadeEixos;
	}

	public void setQuantidadeEixos(Integer quantidadeEixos) {
		this.quantidadeEixos = quantidadeEixos;
	}

	public TipoCargaEnum getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(TipoCargaEnum tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((quantidadeEixos == null) ? 0 : quantidadeEixos.hashCode());
		result = prime * result + ((tipoCarga == null) ? 0 : tipoCarga.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caminhao other = (Caminhao) obj;
		if (quantidadeEixos == null) {
			if (other.quantidadeEixos != null)
				return false;
		} else if (!quantidadeEixos.equals(other.quantidadeEixos))
			return false;
		if (tipoCarga != other.tipoCarga)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " | Caminhao [quantidadeEixos=" + quantidadeEixos + ", tipoCarga=" + tipoCarga + "]";
	}

}
