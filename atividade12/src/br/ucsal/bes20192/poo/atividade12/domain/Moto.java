package br.ucsal.bes20192.poo.atividade12.domain;

public class Moto extends Veiculo {

	private String nomeModelo;

	public Moto(String placa, Integer anoFabricacao, Double valor, String nomeModelo) {
		super(placa, anoFabricacao, valor);
		this.nomeModelo = nomeModelo;
	}

	public String getNomeModelo() {
		return nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nomeModelo == null) ? 0 : nomeModelo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Moto other = (Moto) obj;
		if (nomeModelo == null) {
			if (other.nomeModelo != null)
				return false;
		} else if (!nomeModelo.equals(other.nomeModelo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " | Moto [nomeModelo=" + nomeModelo + "]";
	}

}
