package br.ucsal.bes20192.poo.atividade12.domain;

import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;

public class Carro extends Veiculo {

	// Constantes

	// Atributos

	// Construtores

	// M�todos p�blicos
	// - m�todos espec�ficos
	// - getter/setter
	// - hashcode/equals
	// - toString

	// M�todos privados

	private static final String QTD_PORTAS_INVALIDA_MESSAGE = "A quantidade de portas deve ser maior que zero.";

	private Integer quantidadePortas;

	public Carro(String placa, Integer anoFabricacao, Double valor, Integer quantidadePortas) throws NegocioException {
		super(placa, anoFabricacao, valor);
		setQuantidadePortas(quantidadePortas);
	}

	public Integer getQuantidadePortas() {
		return quantidadePortas;
	}

	public void setQuantidadePortas(Integer quantidadePortas) throws NegocioException {
		validarQuantidadePortas(quantidadePortas);
		this.quantidadePortas = quantidadePortas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((quantidadePortas == null) ? 0 : quantidadePortas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carro other = (Carro) obj;
		if (quantidadePortas == null) {
			if (other.quantidadePortas != null)
				return false;
		} else if (!quantidadePortas.equals(other.quantidadePortas))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " | Carro [quantidadePortas=" + quantidadePortas + "]";
	}

	// Esta valida��o est� aqui na classe trazer esta alternativa. Da perspectiva
	// OO (Orienta��o � Objetos), incluive, � mais aderente.
	// Nas classes BO (business), fizemos as outras valida��es.
	// Num caso concreto, voc� deve escolher uma estrat�gia: ou valida nos Domains
	// ou valida nos Business.
	private void validarQuantidadePortas(Integer quantidadePortas) throws NegocioException {
		if (quantidadePortas <= 0) {
			throw new NegocioException(QTD_PORTAS_INVALIDA_MESSAGE);
		}
	}

}
