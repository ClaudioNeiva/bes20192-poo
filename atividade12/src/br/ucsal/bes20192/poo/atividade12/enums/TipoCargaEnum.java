package br.ucsal.bes20192.poo.atividade12.enums;

public enum TipoCargaEnum {

	LIQUIDOS(1, "L�quidos"), GAS(2, "G�s"), SOLIDOS(3, "S�lidos");

	private Integer codigo;
	private String nome;

	private TipoCargaEnum(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public static TipoCargaEnum valueOfCodigo(Integer codigo) {
		for (TipoCargaEnum tipoCargaEnum : values()) {
			if (tipoCargaEnum.getCodigo().equals(codigo)) {
				return tipoCargaEnum;
			}
		}
		throw new IllegalArgumentException("C�digo " + codigo + " n�o encontrado em " + TipoCargaEnum.class.getName());
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

}
