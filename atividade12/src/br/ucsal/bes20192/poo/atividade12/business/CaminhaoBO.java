package br.ucsal.bes20192.poo.atividade12.business;

import br.ucsal.bes20192.poo.atividade12.domain.Caminhao;
import br.ucsal.bes20192.poo.atividade12.enums.TipoCargaEnum;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;
import br.ucsal.bes20192.poo.atividade12.persistence.VeiculoDAO;

public class CaminhaoBO {

	public static void incluir(Caminhao caminhao) throws NegocioException {
		validar(caminhao);
		VeiculoDAO.incluir(caminhao);
	}

	private static void validar(Caminhao caminhao) throws NegocioException {
		VeiculoBO.validarVeiculo(caminhao);
		validarQuantidadeEixos(caminhao.getQuantidadeEixos(), caminhao.getTipoCarga());
	}

	private static void validarQuantidadeEixos(Integer quantidadeEixos, TipoCargaEnum tipoCarga)
			throws NegocioException {
		if (quantidadeEixos < 0) {
			throw new NegocioException("Quantidade de eixos deve ser maior que zero.");
		}
		if (tipoCarga.equals(TipoCargaEnum.LIQUIDOS) && quantidadeEixos < 4) {
			throw new NegocioException("Ve�culos de carga l�quida deve ter no m�nimo 4 eixos.");
		}
	}

}
