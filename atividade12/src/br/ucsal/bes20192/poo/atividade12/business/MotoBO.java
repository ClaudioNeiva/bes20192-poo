package br.ucsal.bes20192.poo.atividade12.business;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes20192.poo.atividade12.domain.Moto;
import br.ucsal.bes20192.poo.atividade12.domain.Veiculo;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;
import br.ucsal.bes20192.poo.atividade12.persistence.VeiculoDAO;

public class MotoBO {

	public static Set<String> obterNomesModelo() {
		Set<String> nomesModelo = new HashSet<>();
		List<Veiculo> veiculos = VeiculoDAO.obterTodos();
		for (Veiculo veiculo : veiculos) {
			if (veiculo instanceof Moto) {
				String nomeModelo = ((Moto) veiculo).getNomeModelo();
				nomesModelo.add(nomeModelo);
			}
		}
		return nomesModelo;
	}

	public static void incluir(Moto moto) throws NegocioException {
		validar(moto);
		VeiculoDAO.incluir(moto);
	}

	private static void validar(Moto moto) throws NegocioException {
		VeiculoBO.validarVeiculo(moto);
		validarModelo(moto.getNomeModelo());
	}

	private static void validarModelo(String nomeModelo) throws NegocioException {
		if (nomeModelo == null || nomeModelo.trim().isEmpty()) {
			throw new NegocioException("O nome do modelo deve ser informado");
		}
	}

}
