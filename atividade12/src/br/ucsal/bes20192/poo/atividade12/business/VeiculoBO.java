package br.ucsal.bes20192.poo.atividade12.business;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ucsal.bes20192.poo.atividade12.domain.Veiculo;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;
import br.ucsal.bes20192.poo.atividade12.persistence.VeiculoDAO;

public class VeiculoBO {

	public static Map<Integer, Integer> obterQtdVeiculoPorAnoFabricacao() {
		// Map<ano-fabricacao, quantidade-veiculos-para-aquele-ano-fabricacao>
		Map<Integer, Integer> qtdVeiculosAnoFabricacao = new HashMap<>();

		List<Veiculo> veiculos = VeiculoDAO.obterTodos();
		for (Veiculo veiculo : veiculos) {
			if (qtdVeiculosAnoFabricacao.containsKey(veiculo.getAnoFabricacao())) {
				Integer qtdVeiculosAtual = qtdVeiculosAnoFabricacao.get(veiculo.getAnoFabricacao());
				qtdVeiculosAnoFabricacao.put(veiculo.getAnoFabricacao(), qtdVeiculosAtual + 1);
			} else {
				// O ano de fabrica��o n�o estava no mapa, coloco ele no mapa e indico a
				// ocorr�ncia de 1 ve�culo para aquele ano de fabrica��o.
				qtdVeiculosAnoFabricacao.put(veiculo.getAnoFabricacao(), 1);
			}
		}

		return qtdVeiculosAnoFabricacao;
	}

	public static List<Veiculo> obterTodosOrdenadoValor() {
		List<Veiculo> veiculos = VeiculoDAO.obterTodos();
		veiculos.sort(Comparator.comparing(Veiculo::getValor));
		return veiculos;
	}

	public static List<Veiculo> obterTodosOrdenadoPlaca() {
		List<Veiculo> veiculos = VeiculoDAO.obterTodos();
		veiculos.sort(Comparator.comparing(Veiculo::getPlaca));
		return veiculos;
	}

	public static void validarVeiculo(Veiculo veiculo) throws NegocioException {
		validarPlaca(veiculo.getPlaca());
		validarAnoFabricacao(veiculo.getAnoFabricacao());
		validarValor(veiculo.getValor());
	}

	private static void validarPlaca(String placa) throws NegocioException {
		if (placa == null || placa.trim().isEmpty()) {
			throw new NegocioException("A placa deve ser informada.");
		}
	}

	private static void validarValor(Double valor) throws NegocioException {
		if (valor < 0) {
			throw new NegocioException("O valor deve ser maior que zero!");
		}
	}

	private static void validarAnoFabricacao(Integer anoFabricacao) throws NegocioException {
		if (anoFabricacao < 0) {
			throw new NegocioException("O ano de fabrica��o deve ser maior que zero!");
		}
	}

}
