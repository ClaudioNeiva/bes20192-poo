package br.ucsal.bes20192.poo.atividade12.business;

import br.ucsal.bes20192.poo.atividade12.domain.Carro;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;
import br.ucsal.bes20192.poo.atividade12.persistence.VeiculoDAO;

public class CarroBO {

	public static void incluir(Carro carro) throws NegocioException {
		validar(carro);
		VeiculoDAO.incluir(carro);
	}

	private static void validar(Carro carro) throws NegocioException {
		VeiculoBO.validarVeiculo(carro);
	}


}
