package br.ucsal.bes20192.poo.atividade12.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20192.poo.atividade12.domain.Veiculo;

public class VeiculoDAO {

	private static List<Veiculo> veiculos = new ArrayList<>();
	
	public static void incluir(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static List<Veiculo> obterTodos() {
		return new ArrayList<>(veiculos);
	}

}
