package br.ucsal.bes20192.poo.atividade12.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException() {
	}

	public NegocioException(String message) {
		super(message);
	}

}
