package br.ucsal.bes20192.poo.atividade12.tui;

import java.util.List;
import java.util.Map;

import br.ucsal.bes20192.poo.atividade12.business.VeiculoBO;
import br.ucsal.bes20192.poo.atividade12.domain.Veiculo;

public class VeiculoTUI {

	public static void listarTodosOrdenadoValor(){
		List<Veiculo> veiculosOrdenadoValor = VeiculoBO.obterTodosOrdenadoValor();
		System.out.println("Ve�culos por ordem crescente de valor:");
		for(Veiculo veiculo : veiculosOrdenadoValor) {
			System.out.println(veiculo);
		}
	}
	
	public static void listarTodosOrdenadoPlaca(){
		List<Veiculo> veiculosOrdenadoPlaca = VeiculoBO.obterTodosOrdenadoPlaca();
		System.out.println("Ve�culos por ordem crescente de placa:");
		for(Veiculo veiculo : veiculosOrdenadoPlaca) {
			System.out.println(veiculo);
		}
	}
	
	public static void listarQtdVeiculoPorAnoFabricacao() {
		Map<Integer, Integer> qtdVeiculosPorAnoFabricacao = VeiculoBO.obterQtdVeiculoPorAnoFabricacao();
		System.out.println("Quantidade de ve�culos por ano de fabrica��o");
		for (Integer anoFabricacao : qtdVeiculosPorAnoFabricacao.keySet()) {
			System.out.println(anoFabricacao + " x " + qtdVeiculosPorAnoFabricacao.get(anoFabricacao) + " ve�culos");
		}
	}

}
