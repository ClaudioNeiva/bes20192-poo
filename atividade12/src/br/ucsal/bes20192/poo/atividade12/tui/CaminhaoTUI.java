package br.ucsal.bes20192.poo.atividade12.tui;

import br.ucsal.bes20192.poo.atividade12.business.CaminhaoBO;
import br.ucsal.bes20192.poo.atividade12.domain.Caminhao;
import br.ucsal.bes20192.poo.atividade12.enums.TipoCargaEnum;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;

public class CaminhaoTUI {

	public static void incluir() {
		System.out.println("*********** INCLUIR - CAMINH�O ***********");

		String placa;
		Integer anoFabricacao;
		Double valor;
		Integer quantidadeEixos;
		TipoCargaEnum tipoCarga;

		placa = UtilTUI.obterString("Informe a placa:");
		anoFabricacao = UtilTUI.obterInteger("Informe o ano de fabrica��o:");
		valor = UtilTUI.obterDouble("Informe o valor:");
		quantidadeEixos = UtilTUI.obterInteger("Informe a quantidade de eixos:");
		tipoCarga = UtilTUI.obterTipoCargaEnum("Informe o tipo de carga:");

		Caminhao caminhao = new Caminhao(placa, anoFabricacao, valor, quantidadeEixos, tipoCarga);
		try {
			CaminhaoBO.incluir(caminhao);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}

	}

}
