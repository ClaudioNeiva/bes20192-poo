package br.ucsal.bes20192.poo.atividade12.tui;

import java.util.Set;

import br.ucsal.bes20192.poo.atividade12.business.MotoBO;
import br.ucsal.bes20192.poo.atividade12.domain.Moto;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;

public class MotoTUI {

	public static void listarNomesModelo() {
		Set<String> nomesModelo = MotoBO.obterNomesModelo();
		System.out.println("Nomes de modelos para as motos cadastradas:");
		for (String nomeModelo : nomesModelo) {
			System.out.println(nomeModelo);
		}
	}

	public static void incluir() {
		System.out.println("*********** INCLUIR - MOTO ***********");

		String placa;
		Integer anoFabricacao;
		Double valor;
		String nomeModelo;

		placa = UtilTUI.obterString("Informe a placa:");
		anoFabricacao = UtilTUI.obterInteger("Informe o ano de fabricação:");
		valor = UtilTUI.obterDouble("Informe o valor:");
		nomeModelo = UtilTUI.obterString("Informe o nome do modelo:");

		Moto moto = new Moto(placa, anoFabricacao, valor, nomeModelo);
		try {
			MotoBO.incluir(moto);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}

	}

}
