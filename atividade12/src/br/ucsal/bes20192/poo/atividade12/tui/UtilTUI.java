package br.ucsal.bes20192.poo.atividade12.tui;

import java.util.Scanner;

import br.ucsal.bes20192.poo.atividade12.enums.TipoCargaEnum;

public class UtilTUI {

	private static Scanner sc = new Scanner(System.in);

	public static Integer obterInteger(String mensagem) {
		System.out.println(mensagem);
		Integer inteiro = sc.nextInt();
		sc.nextLine();
		return inteiro;
	}

	public static String obterString(String mensagem) {
		System.out.println(mensagem);
		return sc.nextLine();
	}

	public static Double obterDouble(String mensagem) {
		System.out.println(mensagem);
		double valor = sc.nextDouble();
		sc.nextLine();
		return valor;
	}

	public static TipoCargaEnum obterTipoCargaEnum(String mensagem) {
		Integer codigo;
		System.out.println(mensagem);
		exibirTiposCarga();
		codigo = sc.nextInt();
		sc.nextLine();
		return TipoCargaEnum.valueOfCodigo(codigo);
	}

	private static void exibirTiposCarga() {
		for (TipoCargaEnum tipoCarga : TipoCargaEnum.values()) {
			System.out.println(tipoCarga.getCodigo() + "-" + tipoCarga.getNome());
		}
	}

}
