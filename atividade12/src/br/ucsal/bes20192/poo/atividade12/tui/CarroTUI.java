package br.ucsal.bes20192.poo.atividade12.tui;

import br.ucsal.bes20192.poo.atividade12.business.CarroBO;
import br.ucsal.bes20192.poo.atividade12.domain.Carro;
import br.ucsal.bes20192.poo.atividade12.exception.NegocioException;

public class CarroTUI {

	public static void incluir() {
		System.out.println("*********** INCLUIR - CARRO ***********");

		String placa;
		Integer anoFabricacao;
		Double valor;
		Integer quantidadePortas;

		placa = UtilTUI.obterString("Informe a placa:");
		anoFabricacao = UtilTUI.obterInteger("Informe o ano de fabricação:");
		valor = UtilTUI.obterDouble("Informe o valor:");
		quantidadePortas = UtilTUI.obterInteger("Informe a quantidade de portas:");

		Carro carro;
		try {
			carro = new Carro(placa, anoFabricacao, valor, quantidadePortas);
			CarroBO.incluir(carro);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}

	}

}
