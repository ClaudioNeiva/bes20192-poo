package br.ucsal.bes20192.poo.atividade12.tui;

public class Aplicacao {

	public static void main(String[] args) {
		MotoTUI.incluir();
		CarroTUI.incluir();
		CaminhaoTUI.incluir();
		VeiculoTUI.listarTodosOrdenadoPlaca();
		VeiculoTUI.listarTodosOrdenadoValor();
		VeiculoTUI.listarQtdVeiculoPorAnoFabricacao();
		MotoTUI.listarNomesModelo();
	}

}
