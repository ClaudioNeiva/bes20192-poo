package br.ucsal.bes20192.poo.atividade07.veiculo;

public class Exemplo {

	public static void main(String[] args) {

		Moto moto1 = new Moto(
				new Pessoa("12312", "Claudio Neiva",
						new Endereco("12312", "Logradou x", "s/n", "Rua x", "Bairro 1", "Salvador", "BA"), "12312"),
				"XYZ-1234", 2000, 100d, CategoriaMotoEnum.ESTRADA, 2);

		Endereco enderecoClaudio = new Endereco("12312", "Logradou x", "s/n", "Rua x", "Bairro 1", "Salvador", "BA");
		Pessoa pessoaClaudio = new Pessoa("12312", "Claudio Neiva", enderecoClaudio, "12312");
		Moto motoDeClaudio = new Moto(pessoaClaudio, "XYZ-1234", 2000, 100d, CategoriaMotoEnum.ESTRADA, 2);

	}

}
