package br.ucsal.bes20192.poo.atividade07.veiculo;

public class VeiculoCarga extends Veiculo {

	private Integer capacidadeMaximaCarga;

	private Integer qtdEixos;

	private Integer capacidadeTanqueCombustivel;

	public VeiculoCarga(Pessoa proprietario, String placa, Integer anoFabricacao, Double valor,
			Integer capacidadeMaximaCarga, Integer qtdEixos, Integer capacidadeTanqueCombustivel) {
		super(proprietario, placa, anoFabricacao, valor);
		this.capacidadeMaximaCarga = capacidadeMaximaCarga;
		this.qtdEixos = qtdEixos;
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

	public Integer getCapacidadeMaximaCarga() {
		return capacidadeMaximaCarga;
	}

	public void setCapacidadeMaximaCarga(Integer capacidadeMaximaCarga) {
		this.capacidadeMaximaCarga = capacidadeMaximaCarga;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getCapacidadeTanqueCombustivel() {
		return capacidadeTanqueCombustivel;
	}

	public void setCapacidadeTanqueCombustivel(Integer capacidadeTanqueCombustivel) {
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

}
