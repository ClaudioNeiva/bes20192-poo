package br.ucsal.bes20192.poo.atividade07.veiculo;

public class VeiculoPasseio extends Veiculo {

	private Integer qtdMaxPassageiras;

	private Integer capacidadePortaMalas;

	public VeiculoPasseio(Pessoa proprietario, String placa, Integer anoFabricacao, Double valor,
			Integer qtdMaxPassageiras, Integer capacidadePortaMalas) {
		super(proprietario, placa, anoFabricacao, valor);
		this.qtdMaxPassageiras = qtdMaxPassageiras;
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

	public Integer getQtdMaxPassageiras() {
		return qtdMaxPassageiras;
	}

	public void setQtdMaxPassageiras(Integer qtdMaxPassageiras) {
		this.qtdMaxPassageiras = qtdMaxPassageiras;
	}

	public Integer getCapacidadePortaMalas() {
		return capacidadePortaMalas;
	}

	public void setCapacidadePortaMalas(Integer capacidadePortaMalas) {
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

}
