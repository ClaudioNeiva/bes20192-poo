package br.ucsal.bes20192.poo.atividade07.veiculo;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer qtdCilindradas;

	public Moto(Pessoa proprietario, String placa, Integer anoFabricacao, Double valor, CategoriaMotoEnum categoria,
			Integer qtdCilindradas) {
		super(proprietario, placa, anoFabricacao, valor);
		this.categoria = categoria;
		this.qtdCilindradas = qtdCilindradas;
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQtdCilindradas() {
		return qtdCilindradas;
	}

	public void setQtdCilindradas(Integer qtdCilindradas) {
		this.qtdCilindradas = qtdCilindradas;
	}

}
