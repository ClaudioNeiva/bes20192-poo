package br.ucsal.bes20192.poo.atividade07.veiculo;

public class Veiculo {

	private Pessoa proprietario;

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	public Veiculo(Pessoa proprietario, String placa, Integer anoFabricacao, Double valor) {
		super();
		this.proprietario = proprietario;
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
