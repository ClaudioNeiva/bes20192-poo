package br.ucsal.bes20192.poo.aula06;

public class ExemploUsoAtributoClasse {

	public static void main(String[] args) {

		Professor professor1 = new Professor("claudio");
		Professor professor2 = new Professor("maria");

		System.out.println(professor1);
		System.out.println(professor2);

	}

}
