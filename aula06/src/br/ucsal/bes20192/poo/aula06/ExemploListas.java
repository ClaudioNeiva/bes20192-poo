package br.ucsal.bes20192.poo.aula06;

import java.util.ArrayList;
import java.util.List;

public class ExemploListas {

	public static void main(String[] args) {

		List<Professor> professores = new ArrayList<>();

		professores.add(new Professor("Claudio"));
		professores.add(new Professor("Maria"));
		professores.add(new Professor("Pedro"));

		System.out.println("\nProfessor no �ndice 0:");
		System.out.println(professores.get(0));

		System.out.println("\nTodos os professores da lista:");
		System.out.println(professores);

		System.out.println("\nTodos os professores da lista (1 por linha):");
		for (Professor professor : professores) {
			System.out.println(professor);
		}

		// Remover o professor da posi��o 1 (lembrando que � segundo professor
		// da lista):
		professores.remove(1);

		System.out.println("\nTodos os professores da lista (1 por linha):");
		for (Professor professor : professores) {
			System.out.println(professor);
		}

		// Remover todos os professores:
		professores.clear();

		System.out.println("\nTodos os professores da lista (1 por linha):");
		for (Professor professor : professores) {
			System.out.println(professor);
		}

		
	}
}
