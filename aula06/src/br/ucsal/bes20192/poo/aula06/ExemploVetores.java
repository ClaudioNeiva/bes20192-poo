package br.ucsal.bes20192.poo.aula06;

public class ExemploVetores {

	private static final int QTD_PROFESSORES = 5;

	public static void main(String[] args) {

		Professor[] professores = new Professor[QTD_PROFESSORES];
		int posicaoDisponivel = 0;

		professores[posicaoDisponivel] = new Professor("Antonio Claudio");
		posicaoDisponivel++;

		professores[posicaoDisponivel] = new Professor("Maria");
		posicaoDisponivel++;

		professores[posicaoDisponivel] = new Professor("Pedro");
		posicaoDisponivel++;

		System.out.println("Todos os professores cadastrados:");
		for (int i = 0; i < posicaoDisponivel; i++) {
			System.out.println(professores[i]);
		}

		System.out.println("Excluindo o professor da posi��o 1:");
		for (int i = 1; i < QTD_PROFESSORES - 1; i++) {
			professores[i] = professores[i + 1];
		}
		posicaoDisponivel--;

		System.out.println("Todos os professores ap�s a exclus�o do professor da posi��o 1:");
		for (int i = 0; i < posicaoDisponivel; i++) {
			System.out.println(professores[i]);
		}

	}

}
