package br.ucsal.bes20192.poo.atividade04.tui;

import java.util.Scanner;

import br.ucsal.bes20192.poo.atividade04.business.ContatoBO;
import br.ucsal.bes20192.poo.atividade04.domain.Contato;
import br.ucsal.bes20192.poo.atividade04.domain.TipoContatoEnum;
import br.ucsal.bes20192.poo.atividade04.exception.InfraEstruturaException;
import br.ucsal.bes20192.poo.atividade04.exception.NegocioException;

public class ContatoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void incluir() {
		System.out.println("#################### INCLUS�O DE CONTATOS ####################");

		String nome;
		Integer anoNascimento;
		String telefone;
		TipoContatoEnum tipoContato;
		String tipoContatoString;

		System.out.println("Informe os dados do contato:");
		System.out.println("Nome:");
		nome = sc.nextLine();

		System.out.println("Ano de nascimento:");
		anoNascimento = sc.nextInt();
		sc.nextLine();

		System.out.println("Telefone:");
		telefone = sc.nextLine();

		System.out.println("Tipo:");
		tipoContatoString = sc.nextLine();
		// Converter o tipoContatoString em tipoContato
		tipoContato = TipoContatoEnum.valueOf(tipoContatoString.toUpperCase());

		Contato contato = new Contato(nome, anoNascimento, telefone, tipoContato);

		try {
			ContatoBO.incluir(contato);
		} catch (NegocioException e) {
			System.out.println("Erro: " + e.getMessage());
		} catch (InfraEstruturaException e) {
			System.out.println("Erro:" + e.getMessage());
			// Gravar no arquivo de logs.
		}
	}

	public static void excluir() {
		System.out.println("#################### EXCLUS�O DE CONTATOS ####################");
		String nome;

		listar();
		System.out.println("Informe o nome do contato a ser exclu�do:");
		nome = sc.nextLine();

		try {
			ContatoBO.excluir(nome);
		} catch (NegocioException e) {
			e.printStackTrace();
		}
	}

	public static void listar() {
		System.out.println("CONTATOS:");
		Contato[] contatos = ContatoBO.buscarTodos();

		for (Contato contato : contatos) {
			System.out.println("Nome=" + contato.getNome() + " | Telefone=" + contato.getTelefone()
					+ " | Ano de nascimento=" + contato.getAnoNascimento() + " | Tipo=" + contato.getTipo());
		}

	}

	public static void pesquisarNome() {

	}

}
