package br.ucsal.bes20192.poo.atividade04.tui;

public enum OpcoesMenuAgendaEnum {

	INCLUIR(1, "Incluir"),

	EXCLUIR(2, "Excluir"),

	LISTAR(3, "Listar"),

	PESQUISAR_NOME(4, "Pesquisar por nome"),

	SAIR(5, "Sair");

	private Integer codigo;
	private String descricao;

	private OpcoesMenuAgendaEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static OpcoesMenuAgendaEnum valueOfCodigo(Integer codigo) {
		for(OpcoesMenuAgendaEnum opcaoMenuAgenda : values()){
			if(opcaoMenuAgenda.getCodigo().equals(codigo)){
				return opcaoMenuAgenda;
			}
		}
		throw new IllegalArgumentException("O codigo "+codigo+ " n�o � um item da enumera��o.");
	}

}
