package br.ucsal.bes20192.poo.atividade04.tui;

import java.util.Scanner;

public class Agenda {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executarAgenda();
	}

	private static void executarAgenda() {
		OpcoesMenuAgendaEnum opcaoSelecionada;
		do {
			exibirOpcoes();
			opcaoSelecionada = obterOpcaoSelecionada();
			executarOpcaoSelecionada(opcaoSelecionada);
		} while (!opcaoSelecionada.equals(OpcoesMenuAgendaEnum.SAIR));
	}

	private static OpcoesMenuAgendaEnum obterOpcaoSelecionada() {
		Integer opcaoSelecionadaInteger;
		OpcoesMenuAgendaEnum opcaoSelecionada;
		System.out.println("Informe a op��o desejada: ");
		opcaoSelecionadaInteger = sc.nextInt();
		// FIXME Tratar problema com c�digos n�o v�lidos.
		opcaoSelecionada = OpcoesMenuAgendaEnum.valueOfCodigo(opcaoSelecionadaInteger);
		return opcaoSelecionada;
	}

	// TODO Remover depois de estudar o conte�do.
	private static OpcoesMenuAgendaEnum obterOpcaoSelecionadaTexto() {
		String opcaoSelecionadaString;
		OpcoesMenuAgendaEnum opcaoSelecionada;
		System.out.println("Informe a op��o desejada (INCLUIR, EXCLUIR, LISTAR, PESQUISAR_NOME OU SAIR): ");
		opcaoSelecionadaString = sc.nextLine();
		opcaoSelecionada = OpcoesMenuAgendaEnum.valueOf(opcaoSelecionadaString.toUpperCase());
		return opcaoSelecionada;
	}

	private static void executarOpcaoSelecionada(OpcoesMenuAgendaEnum opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case INCLUIR:
			ContatoTUI.incluir();
			break;
		case EXCLUIR:
			ContatoTUI.excluir();
			break;
		case LISTAR:
			ContatoTUI.listar();
			break;
		case PESQUISAR_NOME:
			ContatoTUI.pesquisarNome();
			break;
		default:
			System.out.println("Bye...");
			break;
		}
	}

	private static void exibirOpcoes() {
		for (OpcoesMenuAgendaEnum opcaoMenuAgenda : OpcoesMenuAgendaEnum.values()) {
			System.out.println(opcaoMenuAgenda.getCodigo() + " - " + opcaoMenuAgenda.getDescricao());
		}
	}

}
