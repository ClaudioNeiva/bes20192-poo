package br.ucsal.bes20192.poo.atividade04.business;

import br.ucsal.bes20192.poo.atividade04.domain.Contato;
import br.ucsal.bes20192.poo.atividade04.exception.InfraEstruturaException;
import br.ucsal.bes20192.poo.atividade04.exception.NegocioException;
import br.ucsal.bes20192.poo.atividade04.persistence.ContatoDAO;

public class ContatoBO {

	private static final int TAMANHO_MINIMO_TELEFONE = 8;
	private static final int TAMANHO_MINIMO_NOME = 3;

	public static void incluir(Contato contato) throws InfraEstruturaException, NegocioException {
		validar(contato);
		ContatoDAO.incluir(contato);
	}

	private static void validar(Contato contato) throws NegocioException {
		if (contato.getTelefone() == null || contato.getTelefone().trim().length() < TAMANHO_MINIMO_TELEFONE) {
			throw new NegocioException("Telefone inv�lido!");
		}
		if (contato.getNome() == null || contato.getNome().trim().length() < TAMANHO_MINIMO_NOME) {
			throw new NegocioException("Nome inv�lido!");
		}
	}

	/**
	 * Remover um contato da agenda.
	 * 
	 * @param nome: nome do contato a ser exclu�do.
	 * @throws NegocioException
	 */
	public static void excluir(String nome) throws NegocioException {
		ContatoDAO.excluir(nome);
	}

	public static Contato[] buscarTodos() {
		return ContatoDAO.buscarTodos();
	}

}
