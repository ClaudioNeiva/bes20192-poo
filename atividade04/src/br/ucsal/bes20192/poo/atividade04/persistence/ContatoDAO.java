package br.ucsal.bes20192.poo.atividade04.persistence;

import br.ucsal.bes20192.poo.atividade04.domain.Contato;
import br.ucsal.bes20192.poo.atividade04.exception.InfraEstruturaException;
import br.ucsal.bes20192.poo.atividade04.exception.NegocioException;

public class ContatoDAO {

	private static final int QTD_MAX_CONTATOS = 10;

	private static Contato[] contatos = new Contato[QTD_MAX_CONTATOS];
	private static Integer posicaoDisponivel = 0;

	public static void incluir(Contato contato) throws InfraEstruturaException  {
		if (posicaoDisponivel < QTD_MAX_CONTATOS) {
			contatos[posicaoDisponivel] = contato;
			posicaoDisponivel++;
		} else {
			throw new InfraEstruturaException("Agenda cheia. N�o � poss�vel incluir novos contatos.");
		}
	}

	public static void excluir(String nome) throws NegocioException {
		int posicaoExclusao = pesquisarPosicao(nome);
		excluir(posicaoExclusao);
	}

	private static int pesquisarPosicao(String nome) throws NegocioException {
		for (int i = 0; i < posicaoDisponivel; i++) {
			if (contatos[i].getNome().equalsIgnoreCase(nome)) {
				return i;
			}
		}
		throw new NegocioException("Nome n�o encontrado.");
	}

	private static void excluir(int posicaoExclusao) {
		for (int i = posicaoExclusao; i < posicaoDisponivel - 1; i++) {
			contatos[i] = contatos[i + 1];
		}
		posicaoDisponivel--;
	}

	public static Contato[] buscarTodos() {
		Contato[] contatosPreenchidos = new Contato[posicaoDisponivel];
		for (int i = 0; i < posicaoDisponivel; i++) {
			contatosPreenchidos[i] = contatos[i];
		}
		return contatosPreenchidos;
	}

}
