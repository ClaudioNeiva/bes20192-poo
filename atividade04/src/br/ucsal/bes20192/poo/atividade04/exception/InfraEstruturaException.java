package br.ucsal.bes20192.poo.atividade04.exception;

public class InfraEstruturaException extends Exception {

	private static final long serialVersionUID = 1L;

	public InfraEstruturaException(String mensagem) {
		super(mensagem);
	}

}
