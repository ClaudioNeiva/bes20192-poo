package br.ucsal.bes20192.poo.aula09.monolitica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Scanner sc = new Scanner(System.in);

	private static final String NOME_DO_ARQUIVO = "c:\\trabalho\\alunos.dat";

	private static List<Aluno> alunos = new ArrayList<>();

	private String nome;

	private String email;

	public static void main(String[] args) {
		carregarDados();
		tratarMenu();
	}

	private static void tratarMenu() {
		Integer opcaoSelecionada;
		do {
			exibirOpcoesMenu();
			opcaoSelecionada = sc.nextInt();
			sc.nextLine();
			executarOpcaoSelecionada(opcaoSelecionada);
		} while (opcaoSelecionada != 9);

	}

	private static void executarOpcaoSelecionada(int opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case 1:
			incluir();
			break;
		case 2:
			excluir();
			break;
		case 3:
			consultar();
			break;
		case 9:
			System.out.println("bye...");
			break;

		default:
			break;
		}
	}

	private static void incluir() {
		System.out.println("************** INCLUIR ************** ");
		String nome;
		String email;
		System.out.println("Informe o nome:");
		nome = sc.nextLine();
		System.out.println("Informe o email:");
		email = sc.nextLine();
		Aluno aluno = new Aluno(nome, email);
		try {
			validar(aluno);
			alunos.add(aluno);
			salvarDados();
		} catch (Exception e) {
			System.out.println("Erro ao cadastrar o aluno:" + e.getMessage());
		}
	}

	private static void validar(Aluno aluno) throws Exception {
		if (aluno.getNome() == null || aluno.getNome().trim().length() < 3) {
			throw new Exception("Nome inv�lido!");
		}
		if (aluno.getEmail() == null || aluno.getEmail().trim().length() < 10 || aluno.getEmail().indexOf('@') < 0) {
			throw new Exception("Email inv�lido!");
		}
	}

	private static void consultar() {
		System.out.println("************** CONSULTAR ************** ");
		System.out.println("Alunos cadastrados:");
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}
	}

	private static void excluir() {
		// TODO Auto-generated method stub
	}

	private static void exibirOpcoesMenu() {
		System.out.println("1 - Incluir");
		System.out.println("2 - Excluir");
		System.out.println("3 - Consultar");
		System.out.println("9 - Sair");
	}

	public static void salvarDados() {
		try {
			File file = new File(NOME_DO_ARQUIVO);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(alunos);
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Aluno(String nome, String email) {
		super();
		this.nome = nome;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@SuppressWarnings("unchecked")
	public static void carregarDados() {
		try {
			File file = new File(NOME_DO_ARQUIVO);
			FileInputStream fileInputStream = new FileInputStream(file);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			alunos = (List<Aluno>) objectInputStream.readObject();
			fileInputStream.close();
		} catch (java.io.FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "Aluno [nome=" + nome + ", email=" + email + "]";
	}

}
