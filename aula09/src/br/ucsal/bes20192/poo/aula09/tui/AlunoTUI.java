package br.ucsal.bes20192.poo.aula09.tui;

import java.util.Scanner;

import br.ucsal.bes20192.poo.aula09.business.AlunoBO;
import br.ucsal.bes20192.poo.aula09.domain.Aluno;

public class AlunoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void incluir() {
		System.out.println("************** INCLUIR ************** ");
		String nome;
		String email;
		System.out.println("Informe o nome:");
		nome = sc.nextLine();
		System.out.println("Informe o email:");
		email = sc.nextLine();
		Aluno aluno = new Aluno(nome, email);
		try {
			AlunoBO.inserir(aluno);
		} catch (Exception e) {
			System.out.println("Erro ao cadastrar o aluno:" + e.getMessage());
		}
	}

	public static void consultar() {
		System.out.println("************** CONSULTAR ************** ");
		System.out.println("Alunos cadastrados:");
		for (Aluno aluno : AlunoBO.obterTodos()) {
			System.out.println(aluno);
		}
	}

	public static void excluir() {
		// TODO Auto-generated method stub
	}

}
