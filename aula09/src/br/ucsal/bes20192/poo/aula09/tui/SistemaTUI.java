package br.ucsal.bes20192.poo.aula09.tui;

import java.util.Scanner;

public class SistemaTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		tratarMenu();
	}

	private static void tratarMenu() {
		Integer opcaoSelecionada;
		do {
			exibirOpcoesMenu();
			opcaoSelecionada = sc.nextInt();
			sc.nextLine();
			executarOpcaoSelecionada(opcaoSelecionada);
		} while (opcaoSelecionada != 9);

	}

	private static void executarOpcaoSelecionada(int opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case 1:
			AlunoTUI.incluir();
			break;
		case 2:
			AlunoTUI.excluir();
			break;
		case 3:
			AlunoTUI.consultar();
			break;
		case 9:
			System.out.println("bye...");
			break;

		default:
			break;
		}
	}

	private static void exibirOpcoesMenu() {
		System.out.println("1 - Incluir");
		System.out.println("2 - Excluir");
		System.out.println("3 - Consultar");
		System.out.println("9 - Sair");
	}

}
