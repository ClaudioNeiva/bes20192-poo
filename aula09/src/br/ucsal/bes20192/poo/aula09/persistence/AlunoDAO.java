package br.ucsal.bes20192.poo.aula09.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20192.poo.aula09.domain.Aluno;

public class AlunoDAO {

	private static final String NOME_DO_ARQUIVO = "c:\\trabalho\\alunos.dat";

	private static List<Aluno> alunos = carregarDados();

	public static void inserir(Aluno aluno) {
		alunos.add(aluno);
		salvarDados();
	}

	public static List<Aluno> obterTodos() {
		return alunos;
	}

	public static void salvarDados() {
		try {
			File file = new File(NOME_DO_ARQUIVO);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(alunos);
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Aluno> carregarDados() {
		List<Aluno> alunos = null;
		try {
			File file = new File(NOME_DO_ARQUIVO);
			FileInputStream fileInputStream = new FileInputStream(file);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			alunos = (List<Aluno>) objectInputStream.readObject();
			fileInputStream.close();
			return alunos;
		} catch (java.io.FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

}
