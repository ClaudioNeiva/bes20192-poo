package br.ucsal.bes20192.poo.aula09.business;

import java.util.List;

import br.ucsal.bes20192.poo.aula09.domain.Aluno;
import br.ucsal.bes20192.poo.aula09.persistence.AlunoDAO;

public class AlunoBO {

	public static void inserir(Aluno aluno) throws Exception {
		validar(aluno);
		AlunoDAO.inserir(aluno);
	}

	public static List<Aluno> obterTodos() {
		return AlunoDAO.obterTodos();
	}

	private static void validar(Aluno aluno) throws Exception {
		if (aluno.getNome() == null || aluno.getNome().trim().length() < 3) {
			throw new Exception("Nome inv�lido!");
		}
		if (aluno.getEmail() == null || aluno.getEmail().trim().length() < 10 || aluno.getEmail().indexOf('@') < 0) {
			throw new Exception("Email inv�lido!");
		}
	}

}
