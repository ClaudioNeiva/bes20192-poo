package br.ucsal.bes20192.poo.aula08.ExemploAtributoMetodoClasseInstancia;

public class Correntista {

	private static Integer seq = 0;
	
	private Integer codigo;

	private String nome;

	private String email;

	public Correntista(String nome, String email) {
		super();
		
		seq++;
		this.codigo = seq;
		
		this.nome = nome;
		this.email = email;
	}

	public static Integer getSeq(){
		return seq;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getCodigo() {
		return codigo;
	}

}
