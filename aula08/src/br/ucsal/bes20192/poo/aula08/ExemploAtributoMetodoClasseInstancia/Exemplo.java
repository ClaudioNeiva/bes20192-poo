package br.ucsal.bes20192.poo.aula08.ExemploAtributoMetodoClasseInstancia;

public class Exemplo {

	public static void main(String[] args) {

		System.out.println("O valor de Correntista.seq � " + Correntista.getSeq());

		Correntista correntista1 = new Correntista("Claudio Neiva", "claudio@ucsal.br");

		Correntista correntista2 = new Correntista("Maria da Silva", "maria@ucsal.br");

		Correntista correntista3 = correntista2;

	}

}