package br.ucsal.bes20192.poo.aula07;

public class ExemploAtributoMetodoClasse {

	public static void main(String[] args) {

		Professor professor1 = new Professor("claudio");
		Professor professor2 = new Professor("antonio");
		Professor professor3 = new Professor("neiva");

		System.out.println("professor1.matricula=" + professor1.getMatricula());
		System.out.println("professor2.matricula=" + professor2.getMatricula());
		System.out.println("professor3.matricula=" + professor3.getMatricula());

		System.out.println("contador=" + Professor.getContador());
	}

}
