package br.ucsal.bes20192.poo.aula07;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ExemploLista {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		List<String> nomes;

		System.out.println("Voc� deseja utilizar um ArrayList (A) ou um LinkedList (L)?");
		String resposta = sc.nextLine();
		if (resposta.equalsIgnoreCase("A")) {
			nomes = new ArrayList<>();
		} else {
			nomes = new LinkedList<>();
		}

		System.out.println("classe de nomes=" + nomes.getClass().getCanonicalName());

		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("neiva");

		System.out.println("Nomes na lista:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("Nomes na lista:");
		for (int i = 0; i < nomes.size(); i++) {
			String nome = nomes.get(i);
			System.out.println(nome);
		}

		System.out.println("Lista=" + nomes);

		// Uma lista cujo tipo armazenado seja Object pode referenciar
		// inst�ncias de qualquer classe.
		List<Object> objetos = new ArrayList<>();
		objetos.add(123123);
		objetos.add("claudio");
		objetos.add(true);

	}

}
