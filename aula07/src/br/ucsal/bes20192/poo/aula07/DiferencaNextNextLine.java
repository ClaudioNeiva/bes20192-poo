package br.ucsal.bes20192.poo.aula07;

import java.util.Scanner;

public class DiferencaNextNextLine {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterStringUsandoNextLine();
		obterStringUsandoNext();
	}

	private static void obterStringUsandoNextLine() {
		System.out.println("******************** Usando o NEXTLINE ********************");

		String nome1;
		String nome2;

		System.out.println("informe o nome1:");
		nome1 = sc.nextLine();
		System.out.println("informe o nome2:");
		nome2 = sc.nextLine();

		System.out.println("nome1=" + nome1);
		System.out.println("nome2=" + nome2);
	}

	private static void obterStringUsandoNext() {
		System.out.println("******************** Usando o NEXT ********************");

		String nome1;
		String nome2;

		System.out.println("informe o nome1:");
		nome1 = sc.next();
		System.out.println("informe o nome2:");
		nome2 = sc.next();

		System.out.println("nome1=" + nome1);
		System.out.println("nome2=" + nome2);
	}
}
