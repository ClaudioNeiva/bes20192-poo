package br.ucsal.bes20192.poo.aula07.comheranca;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica();
		pessoaFisica1.cpf = "123123";
		pessoaFisica1.endereco = "Rua x";

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica();
		pessoaJuridica1.cnpj = "678678";
		pessoaJuridica1.endereco = "Rua z";

		Pessoa pessoa = new Pessoa();
		pessoa.endereco = "Rua y";

	}

}
