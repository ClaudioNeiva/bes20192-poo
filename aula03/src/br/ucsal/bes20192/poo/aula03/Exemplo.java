package br.ucsal.bes20192.poo.aula03;

public class Exemplo {

	public static void main(String[] args) {
		// instanciarAlunos();
		instanciarProfessores();
	}

	static void instanciarProfessores() {
		Professor professor1 = new Professor();
		Professor professor2 = new Professor();
		Professor professor3 = professor1;

		professor1.nome = "Claudio";
		professor1.titulacao = "Especialista";

		professor2.nome = "Claudio";
		professor2.titulacao = "Especialista";

		if (professor1 == professor2) {
			System.out.println("professor1 aponta para a mesma inst�ncia apontada por professor2");
		} else {
			System.out.println("professor1 aponta para uma inst�ncia diferente da apontada por professor2");
		}

		if (professor1 == professor3) {
			System.out.println("professor1 aponta para a mesma inst�ncia apontada por professor3");
		} else {
			System.out.println("professor1 aponta para uma inst�ncia diferente da apontada por professor3");
		}

		if (professor1.equals(professor2)) {
			System.out.println("professor1 � igual ao professor2");
		} else {
			System.out.println("professor1 � diferente ao professor2");
		}

	}

	static void instanciarAlunos() {

		String[] nomes = new String[10];
		int[] notas = new int[10];
		Aluno[] alunos = new Aluno[10];
		for (int i = 0; i < 10; i++) {
			alunos[i] = new Aluno();
		}

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = new Aluno();

		aluno1.matricula = 123;
		aluno1.nome = "Claudio";
		aluno1.anoMatricula = 2018;

		aluno2.matricula = 123;
		aluno2.nome = "Claudio";
		aluno2.anoMatricula = 2018;

		aluno3.nome = "Pedro";
		aluno3.matricula = 789;
		aluno3.anoMatricula = 2019;

		if (aluno1 == aluno2) {
			System.out.println("aluno1 aponta para a mesma inst�ncia apontada por aluno2");
		} else {
			System.out.println("alunos aponta para uma inst�ncia diferente da apontada por aluno2");
		}

		apresentarAluno(aluno1);
		apresentarAluno(aluno2);
		apresentarAluno(aluno3);

	}

	private static void apresentarAluno(Aluno aluno) {
		System.out.println("****************************");
		System.out.println("matricula=" + aluno.matricula);
		System.out.println("nome=" + aluno.nome);
		System.out.println("email=" + aluno.email);
		System.out.println("situacao=" + aluno.situacao);
		System.out.println("anoMatricula=" + aluno.anoMatricula);
	}

}
