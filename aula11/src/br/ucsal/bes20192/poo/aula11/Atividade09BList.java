package br.ucsal.bes20192.poo.aula11;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Atividade09BList {

	private static final int QTD_NOMES = 20;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNomesEExibirDistintosQtd();
	}

	private static void obterNomesEExibirDistintosQtd() {
		List<String> nomesDistintos = new ArrayList<>();
		List<Integer> qtds = new ArrayList<>();
		System.out.println("Informe " + QTD_NOMES + " nomes:");
		for (int i = 0; i < QTD_NOMES; i++) {
			String nome = sc.nextLine();

			int posicaoNome = nomesDistintos.indexOf(nome);
			if (posicaoNome < 0) {
				// O nome n�o foi informado ainda
				nomesDistintos.add(nome);
				qtds.add(1);
			} else {
				// O nome j� foi informado e � necess�rio incrementar a quantidade de
				// ocorr�ncias do mesmo.
				qtds.set(posicaoNome, qtds.get(posicaoNome) + 1);
			}

		}
		System.out.println("Nomes informados e suas respectivas quantidades:");
		for (int i = 0; i < nomesDistintos.size(); i++) {
			System.out.println(nomesDistintos.get(i) + " x " + qtds.get(i));
		}
	}

}
