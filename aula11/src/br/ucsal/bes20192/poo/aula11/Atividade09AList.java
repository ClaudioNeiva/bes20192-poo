package br.ucsal.bes20192.poo.aula11;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Atividade09AList {

	private static final int QTD_NOMES = 10;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		List<String> nomes = obterNomesDistintosUsandoList();
		exibirNomes(nomes);
	}

	private static List<String> obterNomesDistintosUsandoList() {
		List<String> nomes = new ArrayList<>();
		System.out.println("Informe " + QTD_NOMES + " nomes distintos:");
		do {
			String nome = sc.nextLine();
			if (!nomes.contains(nome)) {     
				nomes.add(nome);
			}
		} while (nomes.size() < QTD_NOMES);
		return nomes;
	}

	private static void exibirNomes(List<String> nomes) {
		System.out.println("Nomes informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
