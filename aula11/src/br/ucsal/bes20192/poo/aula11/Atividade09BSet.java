package br.ucsal.bes20192.poo.aula11;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Atividade09BSet {

	private static final int QTD_NOMES = 10;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Set<String> nomes = obterNomesDistintosUsandoList();
		exibirNomes(nomes);
	}

	private static Set<String> obterNomesDistintosUsandoList() {
		// LinkedHashSet criar um conjunto que mantem os elementos na mesma ordem que os
		// mesmos foram adicionados.
		// Set<String> nomes = new LinkedHashSet<String>();

		// TreeSet ordenar os elementos adicionados ao conjunto.
		// Set<String> nomes = new TreeSet<>();

		Set<String> nomes = new HashSet<>();
		System.out.println("Informe " + QTD_NOMES + " nomes distintos:");
		do {
			String nome = sc.nextLine();
			nomes.add(nome);
		} while (nomes.size() < QTD_NOMES);
		return nomes;
	}

	private static void exibirNomes(Set<String> nomes) {
		System.out.println("Nomes informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
