package br.ucsal.bes20192.poo.aula11;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Atividade09BMap {

	private static final int QTD_NOMES = 20;

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNomesEExibirDistintosQtd();
	}

	private static void obterNomesEExibirDistintosQtd() {
		// Map <nome, qtd-ocorr�ncias>
		Map<String, Integer> nomesQtds = new HashMap<>();

		System.out.println("Informe " + QTD_NOMES + " nomes:");
		for (int i = 0; i < QTD_NOMES; i++) {
			String nome = sc.nextLine();

			if (nomesQtds.containsKey(nome)) {
				// Atualizar a quantidade de ocorr�ncias do nome
				Integer qtdAtual = nomesQtds.get(nome);
				nomesQtds.put(nome, qtdAtual + 1);
			} else {
				// Adicionar o nome ao mapa, definindo que a quantidade ocorr�ncias � 1.
				nomesQtds.put(nome, 1);
			}

		}
		System.out.println("Nomes informados e suas respectivas quantidades:");
		for (String nome : nomesQtds.keySet()) {
			System.out.println(nome + " x " + nomesQtds.get(nome));
		}
	}

}
