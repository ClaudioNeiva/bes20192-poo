package br.ucsal.bes20192.poo.aula13.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoJava8MinhasClasses {

	public static void main(String[] args) {

		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("maria", 2000, "ester"));
		pessoas.add(new Pessoa("clara", 2001, "lais"));
		pessoas.add(new Pessoa("pedro", 1990, "ester"));
		pessoas.add(new Pessoa("ana", 2005, "lais"));
		pessoas.add(new Pessoa("joao", 2009, "leila"));
		pessoas.add(new Pessoa("manuela", 2000, "lais"));

		System.out.println("\nLista original:");
		pessoas.forEach(System.out::println);

		// Collections.sort(pessoas);
		pessoas.sort(Comparator.naturalOrder());
		System.out.println("\nLista em ordem crescente de nome da pessoa:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Utilizando a estrat�gia do Java 7 e anteriores
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa p1, Pessoa p2) {
				return p1.getAnoNascimento().compareTo(p2.getAnoNascimento());
			}
		});

		// Utilizando a estrat�gia do Java 8 pra frente - instanciando inline o m�todo
		// compare, atrav�s de infer�ncia
		pessoas.sort((Pessoa p1, Pessoa p2) -> p1.getAnoNascimento().compareTo(p2.getAnoNascimento()));

		// Utilizando a estrat�gia do Java 8 pra frente - utilizando comparadores
		// provido pelo Comparator
		pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento));

		System.out.println("\nLista em ordem crescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Collections.sort(pessoas, new Comparator<Pessoa>() {
		// @Override
		// public int compare(Pessoa p1, Pessoa p2) {
		// int resultado = p1.getNomeMae().compareTo(p2.getNomeMae());
		// if (resultado == 0) {
		// resultado = p1.getNome().compareTo(p2.getNome());
		// }
		// return resultado;
		// }
		// });
		pessoas.sort(Comparator.comparing(Pessoa::getNomeMae).thenComparing(Pessoa::getNome));
		System.out
				.println("\nLista em ordem crescente do nome da m�e e, para a mesma m�e, por ordem crescente de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
	}

}
