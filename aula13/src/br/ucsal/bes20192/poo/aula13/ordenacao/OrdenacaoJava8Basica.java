package br.ucsal.bes20192.poo.aula13.ordenacao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoJava8Basica {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("clara");
		nomes.add("ana");
		nomes.add("claudio");
		nomes.add("pedro");
		nomes.add("joao");

		System.out.println("\nLista original:");
		System.out.println(nomes);

		nomes.sort(Comparator.naturalOrder());
		System.out.println("\nLista em ordem crescente de nome:");
		System.out.println(nomes);

	}

}
