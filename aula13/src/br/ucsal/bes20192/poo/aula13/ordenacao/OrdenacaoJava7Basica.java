package br.ucsal.bes20192.poo.aula13.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdenacaoJava7Basica {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("clara");
		nomes.add("ana");
		nomes.add("claudio");
		nomes.add("pedro");
		nomes.add("joao");

		System.out.println("\nLista original:");
		System.out.println(nomes);

		Collections.sort(nomes);
		System.out.println("\nLista em ordem crescente de nome:");
		System.out.println(nomes);

		// O reverse n�o ordena, ele simplesmente inverte a lista, como ela j� estava
		// ordenada (ordem crescente), depois do reverse a ordem ficou decrescente.
		Collections.reverse(nomes);
		System.out.println("\nLista em ordem decrescente de nome:");
		System.out.println(nomes);

	}

}
