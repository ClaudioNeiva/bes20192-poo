package br.ucsal.bes20192.poo.aula13.interface1;

public class Auxiliar extends Pessoa implements Funcionario {

	private String ctps;

	public Auxiliar(String nome, Integer anoNascimento, String nomeMae, String ctps) {
		super(nome, anoNascimento, nomeMae);
		this.ctps = ctps;
	}

	public String getCtps() {
		return ctps;
	}

	public void setCtps(String ctps) {
		this.ctps = ctps;
	}

	@Override
	public void exibirSalario() {
		System.out.println(20000);
	}

}
