package br.ucsal.bes20192.poo.aula13.interface1;

import java.util.ArrayList;
import java.util.List;

public class DeptoPessoa {

	public static void main(String[] args) {

		Funcionario funcionario1 = new Medico("Claudio", 2000, "Lais", 34565);
		funcionario1.exibirUmaCoisa();
		
		List<Funcionario> funcionarios = new ArrayList<>();

		funcionarios.add(new Medico("", 2000, "", 12));
		funcionarios.add(new Enfermeiro("", 2000, "",4562));
		funcionarios.add(new Auxiliar("", 2000, "", "34534"));
		
		for (Funcionario funcionario : funcionarios) {
			exibirSalario(funcionario);
		}

	}

	private static void exibirSalario(Funcionario funcionario) {
		funcionario.exibirSalario();
	}

}
