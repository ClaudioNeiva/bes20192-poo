package br.ucsal.bes20192.poo.aula13.interface1;

public interface ProssionalEspecializado {

	void exibirTitulacao();

	default void exibirUmaCoisa() {
		System.out.println("Apenas outra coisa");
	}

}
