package br.ucsal.bes20192.poo.aula13.interface1;

public class Medico extends Pessoa implements Funcionario, ProssionalEspecializado {

	private Integer crm;

	public Medico(String nome, Integer anoNascimento, String nomeMae, Integer crm) {
		super(nome, anoNascimento, nomeMae);
		this.crm = crm;
	}

	public Integer getCrm() {
		return crm;
	}

	public void setCrm(Integer crm) {
		this.crm = crm;
	}

	@Override
	public void exibirSalario() {
		System.out.println(30000);
	}

	@Override
	public void exibirTitulacao() {
		System.out.println("Sei fazer tudo!");
	}

	@Override
	public void exibirUmaCoisa() {
		ProssionalEspecializado.super.exibirUmaCoisa();
	}

}
