package br.ucsal.bes20192.poo.aula13.interface1;

public class Enfermeiro extends Pessoa implements Funcionario {

	private Integer coren;

	public Enfermeiro(String nome, Integer anoNascimento, String nomeMae, Integer coren) {
		super(nome, anoNascimento, nomeMae);
		this.coren = coren;
	}

	public Integer getCoren() {
		return coren;
	}

	public void setCoren(Integer coren) {
		this.coren = coren;
	}

	@Override
	public void exibirSalario() {
		System.out.println(35000);
	}

}
