package br.ucsal.bes20192.poo.aula13.interface1;

public class Paciente extends Pessoa {

	private Integer numeroConvenio;

	public Paciente(String nome, Integer anoNascimento, String nomeMae, Integer numeroConvenio) {
		super(nome, anoNascimento, nomeMae);
		this.numeroConvenio = numeroConvenio;
	}

	public Integer getNumeroConvenio() {
		return numeroConvenio;
	}

	public void setNumeroConvenio(Integer numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}

}
