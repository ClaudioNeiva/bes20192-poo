package br.ucsal.bes20192.poo.aula13.interface1;

public interface Funcionario {

	void exibirSalario();

	default void exibirUmaCoisa() {
		System.out.println("Apenas uma coisa");
	}
	
}
