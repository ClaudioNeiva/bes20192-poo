package br.ucsal.bes20192.poo.aula04;

public class Professor {

	private Integer matricula;

	private String nome;

	private String email;

	private String situacao;

	private String titulacao;

	private Integer anoNascimento;

	public Professor() {
		situacao = "ATIVO";
	}

	// Professor(Integer anoNascimento) {
	// this.anoNascimento = anoNascimento;
	// situacao = "ATIVO";
	// }

	public Professor(String nome) {
		if (nome == null) {
			throw new RuntimeException("Professor tem que ter nome... Erro grave ... bye...");
		}
		this.nome = nome;
		situacao = "ATIVO";
	}

	public Professor(String nome, String titulacao) {
		this.nome = nome;
		this.titulacao = titulacao;
		situacao = "ATIVO";
	}

	public Professor(String nome, Integer anoNascimento) {
		this.nome = nome;
		this.anoNascimento = anoNascimento;
	}

	public Professor(Integer anoNascimentoProfessor, String situacaoProfessor) {
		situacao = situacaoProfessor;
		anoNascimento = anoNascimentoProfessor;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(String titulacao) {
		this.titulacao = titulacao;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		result = prime * result + ((titulacao == null) ? 0 : titulacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (situacao == null) {
			if (other.situacao != null)
				return false;
		} else if (!situacao.equals(other.situacao))
			return false;
		if (titulacao == null) {
			if (other.titulacao != null)
				return false;
		} else if (!titulacao.equals(other.titulacao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Professor [matricula=" + matricula + ", nome=" + nome + ", email=" + email + ", situacao=" + situacao
				+ ", titulacao=" + titulacao + ", anoNascimento=" + anoNascimento + "]";
	}

}
