package br.ucsal.bes20192.poo.atividade10.exemplo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.ucsal.bes20192.poo.atividade10.domain.Aluno;
import br.ucsal.bes20192.poo.atividade10.domain.Disciplina;
import br.ucsal.bes20192.poo.atividade10.domain.TipoDisciplinaEnum;
import br.ucsal.bes20192.poo.atividade10.domain.Turma;

public class Exemplo2 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(678, "Claudio", "neiva@ucsal.br");
		Aluno aluno2 = new Aluno(356, "Maria", "maria@ucsal.br");
		Aluno aluno3 = new Aluno(679, "Ana", "caju@ucsal.br");
		Aluno aluno4 = new Aluno(845, "Pedro", "jaca@ucsal.br");

		Disciplina disciplinaPOO = new Disciplina("BES008", "Programa��o Orientada � Objetos",
				TipoDisciplinaEnum.ESPECIFICA);
		Disciplina disciplinaBD = new Disciplina("BES010", "Banco de Dados", TipoDisciplinaEnum.ESPECIFICA);
		Disciplina disciplinaIAP = new Disciplina("UCS854", "Inicia��o ao Pensar", TipoDisciplinaEnum.EIXO);

		Turma turma1 = new Turma(2019, 2, disciplinaPOO);
		turma1.getAlunos().add(aluno1);
		turma1.getAlunos().add(aluno2);
		turma1.getAlunos().add(aluno3);

		Turma turma2 = new Turma(2019, 2, disciplinaIAP);
		turma2.getAlunos().add(aluno2);
		turma2.getAlunos().add(aluno4);

		Turma turma3 = new Turma(2019, 2, disciplinaBD);
		turma3.getAlunos().add(aluno1);
		turma3.getAlunos().add(aluno2);
		turma3.getAlunos().add(aluno3);
		turma3.getAlunos().add(aluno4);

		// Map<tipo-disciplina, quantidade de alunos matriculados>
		Map<TipoDisciplinaEnum, Integer> qtdAlunosPorTipoDisciplina = new HashMap<>();
		contablilizarAlunos(qtdAlunosPorTipoDisciplina, turma1);
		contablilizarAlunos(qtdAlunosPorTipoDisciplina, turma2);
		contablilizarAlunos(qtdAlunosPorTipoDisciplina, turma3);

		System.out.println("Tipo de disciplina x qtd de alunos matriculados:");
		for (TipoDisciplinaEnum tipoDisciplina : qtdAlunosPorTipoDisciplina.keySet()) {
			System.out.println(tipoDisciplina + " x " + qtdAlunosPorTipoDisciplina.get(tipoDisciplina));
		}

	}

	private static void contablilizarAlunos(Map<TipoDisciplinaEnum, Integer> mapa, Turma turma) {
		TipoDisciplinaEnum tipoDisciplina = turma.getDisciplina().getTipoDisciplina();
		Integer qtdAtual;
		if (mapa.containsKey(tipoDisciplina)) {
			qtdAtual = mapa.get(tipoDisciplina);
		} else {
			qtdAtual = 0;
		}
		qtdAtual += turma.getAlunos().size();
		mapa.put(tipoDisciplina, qtdAtual);
	}

}
