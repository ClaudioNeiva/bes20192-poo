package br.ucsal.bes20192.poo.atividade10.exemplo;

import br.ucsal.bes20192.poo.atividade10.domain.Aluno;
import br.ucsal.bes20192.poo.atividade10.domain.Disciplina;
import br.ucsal.bes20192.poo.atividade10.domain.TipoDisciplinaEnum;
import br.ucsal.bes20192.poo.atividade10.domain.Turma;

public class Exemplo1 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(678, "Claudio", "neiva@ucsal.br");
		Aluno aluno2 = new Aluno(356, "Maria", "maria@ucsal.br");
		Aluno aluno3 = new Aluno(679, "Ana", "caju@ucsal.br");
		Aluno aluno4 = new Aluno(845, "Pedro", "jaca@ucsal.br");
		Aluno aluno5 = new Aluno(678, "Claudio", "neiva@ucsal.br");

		Disciplina disciplinaPOO = new Disciplina("BES008", "Programa��o Orientada � Objetos",
				TipoDisciplinaEnum.ESPECIFICA);
		Disciplina disciplinaBD = new Disciplina("BES010", "Banco de Dados", TipoDisciplinaEnum.ESPECIFICA);
		Disciplina disciplinaIAP = new Disciplina("UCS854", "Inicia��o ao Pensar", TipoDisciplinaEnum.EIXO);

		Turma turma1 = new Turma(2019, 2, disciplinaPOO);
		System.out.println("aluno1.hashCode()=" + aluno1.hashCode());
		turma1.getAlunos().add(aluno1);
		turma1.getAlunos().add(aluno2);
		turma1.getAlunos().add(aluno3);
		turma1.getAlunos().add(aluno5);

		System.out.println(" **************************** ANTES de mudar o nome do aluno1");
		System.out.println("Alunos:");
		for (Aluno aluno : turma1.getAlunos()) {
			System.out.println(aluno.toString());
		}
		System.out.println("aluno1=" + aluno1);
		if (turma1.getAlunos().contains(aluno1)) {
			System.out.println("Sim, turma1 contem aluno1.");
		} else {
			System.out.println("N�O, turma1 N�O contem aluno1.");
		}

		System.out.println(" **************************** DEPOIS de mudar o nome do aluno1");

		aluno1.setNome("Antonio Claudio");
		System.out.println("aluno1.hashCode()=" + aluno1.hashCode());

		System.out.println("Alunos:");
		for (Aluno aluno : turma1.getAlunos()) {
			System.out.println(aluno.toString());
		}
		
		System.out.println("aluno1=" + aluno1);
		if (turma1.getAlunos().contains(aluno1)) {
			System.out.println("Sim, turma1 contem aluno1.");
		} else {
			System.out.println("N�O, turma1 N�O contem aluno1.");
		}

		System.out.println(" **************************** MAIS ESTRANHO AINDA!");
		for (Aluno aluno : turma1.getAlunos()) {
			if (turma1.getAlunos().contains(aluno)) {
				System.out.println("Sim, turma1 contem aluno." + aluno.getNome());
			} else {
				System.out.println("N�O, turma1 N�O contem aluno." + aluno.getNome());
			}

		}
	}

}
